const express = require('express')
const { getAllProducts, getProductById, addNewProduct, updateProduct, deleteProduct } = require('../controllers/productController')
const router = express.Router()

// Get all products - GET
router.get('/', getAllProducts)

// Get a product - GET
router.get('/:productId', getProductById)

// Add new product - POST
router.post('/', addNewProduct)

// Update a product - PATCH
router.patch('/:productId', updateProduct)

// Delete a product - DELETE
router.delete('/:productId', deleteProduct)

module.exports = router