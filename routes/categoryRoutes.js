const express = require('express')
const { getAllCategories, getCategoryById, addNewCategory, updateCategory, deleteCategory } = require('../controllers/categoryController')
const router = express.Router()

// Get all categories 
router.get('/', getAllCategories)

// Get one category
router.get('/:categoryId', getCategoryById)

// Add new category
router.post('/', addNewCategory)

// Update a category
router.patch('/:categoryId', updateCategory)

// Delete a category
router.delete('/:categoryId', deleteCategory)

module.exports = router