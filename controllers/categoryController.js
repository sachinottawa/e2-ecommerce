const getAllCategories = async (req, res) => {
    res.status(404).send("Not written")
}

const getCategoryById = async (req, res) => {
    res.status(404).send("Not written")
}

const addNewCategory = async (req, res) => {
    res.status(404).send("Not written")
}

const updateCategory = async (req, res) => {
    res.status(404).send("Not written")
}

const deleteCategory = async (req, res) => {
    res.status(404).send("Not written")
}

module.exports = {
    getAllCategories,
    getCategoryById,
    addNewCategory,
    updateCategory,
    deleteCategory
}