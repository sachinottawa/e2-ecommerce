const getAllProducts = async (req, res) => {
    res.status(404).send("Not written")
}

const getProductById = async (req, res) => {
    res.status(404).send("Not written")
}

const addNewProduct = async (req, res) => {
    res.status(404).send("Not written")
}

const updateProduct = async (req, res) => {
    res.status(404).send("Not written")
}

const deleteProduct = async (req, res) => {
    res.status(404).send("Not written")
}

module.exports = {
    getAllProducts,
    getProductById,
    addNewProduct,
    updateProduct,
    deleteProduct
}